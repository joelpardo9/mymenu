import React from "react";
import logo from "./logo.jpg";
import { Card } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import './CartWidget.css';

function CartWidget() {
  return (
  
    <div className="App">
      <Card border="dark">
        <Card.Img     
        alt="logo empresa"
        src={logo}       
        width="40"
        height="40"
        className="d-inline-block align-top"         
        />
      </Card>
      </div>
   
  );
}

export default CartWidget;
