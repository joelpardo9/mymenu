import cervezaAmericanIPA from "./AmericanIPA.png"
import cervezaKolsch from "./CasteloKolsch.png"
import cervezaDobleIpa from "./DobleIpa.png"


 export const itemsData=[
           {
             id: 0,
             title: "Cerveza Bulgara American IPA 473ml",
             descripcion: "La Cerveza Bulgara es una IPA Americana con un intenso aroma a lúpulos americanos que aportan notas cítricas y a frutas tropicales." ,
             price: "$220",
             cantidad: "15",
             destacado: true,
             imagen: cervezaAmericanIPA,
           },
           {
             id: 1,
             title: "Cerveza Castelo Kölsch 473ml",
             descripcion: "La Cerveza Castelo Kölsch es liviana y muy refrescante. Tiene un brillante color dorado y espuma blanca sostenida.",
             price: "$175",
             cantidad: "20",
             destacado: true,
             imagen: cervezaKolsch,
           },
           {
             id: 2,
             title: "Cerveza Bronx Imperial Doble IPA 473ml",
             descripcion: "La Cerveza Bronx Imperial es mas que una IPA, son dos IPAs juntas! Es fuerte y aromática, con cuerpo para darle soporte.",
             price: "$260",
             cantidad: "30",
             destacado: true,
             imagen: cervezaDobleIpa,
           }
         ]
 