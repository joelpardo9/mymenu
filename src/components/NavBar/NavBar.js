import React from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import CartWidget from "../CartWidget/CartWidget";

function NavBar(props) {
  return (
    <div className="App">
      <Navbar bg="light" expand="lg">
        <CartWidget/>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <NavDropdown title="Cervezas" id="basic-nav-dropdown">
              <NavDropdown.Item href="#lager">Lager</NavDropdown.Item>
              <NavDropdown.Item href="#trigo">Trigo</NavDropdown.Item>
              <NavDropdown.Item href="#stout">Stout</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="Especialidades" id="basic-nav-dropdown">
              <NavDropdown.Item href="#nuggets">Nuggets de pollo caseros</NavDropdown.Item>
              <NavDropdown.Item href="#bastonesMozzarella">Bastones de mozzarella</NavDropdown.Item>
              <NavDropdown.Item href="#Rolls">Rolls de jamón y queso</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#papasGajo">Papas en gajos</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="Vinos" id="basic-nav-dropdown">
              <NavDropdown.Item href="#malbec">Malbec</NavDropdown.Item>
              <NavDropdown.Item href="#sirah">Sirah</NavDropdown.Item>
              <NavDropdown.Item href="#cosechatardia">Cosecha tardía</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#Chardonnay">Chardonnay
              </NavDropdown.Item>
            </NavDropdown>                   
            <Nav.Link href="#quienesomos">Quienes Somos?</Nav.Link>
          </Nav>          
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
}

export default NavBar;